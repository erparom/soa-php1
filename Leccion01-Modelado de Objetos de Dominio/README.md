#SOA PHP#
##Lección 1-Modelado de Objetos de Dominio##

###Introducción###

En este primer tutorial, veremos lo que significa una capa de servicios (Service Layer) y cómo usarla en conjunción con el patrón de arquitectura **MVC (Modelo-Vista-Controlador)**.
Este tipo de arquitectura nos permitirá construir aplicaciones multi-capa, capaces de funcionar cun múltiples clientes.

Vamos a tomar un enfoque minimalista a un miniframework MVC con el siguiente flujo:

1-Una vez que un **REQUEST** ha sido disparado en la interfaz de usuario, el **Front Controller** hace un matcheo de rutas y despacha el REQUEST a la accion apropiada del controlador requerido, junto con determinados parámetros.
2-En turno, la acción del controlador toma uno o mas modelos **(modelos de dominio o entidades)** y los usa para realizar operaciones **CRUD** en la **capa de persistencia** (usualmente un **RDBMS** o **ORM**), pero puede hacer las operaciones a través de un servicio o cualquier otra capa intermedia.
3-Finalmente, la interface de usuario es actualizada en función de la vista que el controlador pase, junto con los parámetros de retorno.
4-Dicho proceso prosigue continuamente en cada petición del usuario.

Mientras que este esquema es eficiente en determinados **casos de uso** sencillos, no se escala fácilmente. Cuándo una aplicación crece en tamaño y complejidad, es necesario tener un acercamiento mas flexible. Esto incluye diversas técnicas como el decople (**decoupling**) de los modelos a la capa de persistencia, así como de implementar capas de mapeo relacional (**relational mapping**) y capas de repositorio (**model/entity repositories**).
Incuso, es posible pensar en un patrón mas sofisticado, como DML (*Domain Model Logic*), cuyos participantes que son entidades con **persistencia agnóstica**.
, are persistence agnostic.

El escenario puede tornarse aún mas complicado, especialm ente cuándo es necesario realizar tareas adicionales como extraer datos de un API externa (como Twitter o Facebook) o se requiere interactuar con distintos **Frontends** (clientes).
Si se procede a implementar los pasos mencionados previamente con un modelo MVC y acciones en controladores por cada escenario, se creará un controlador gigantezco, con decenas o cientos de métodos, con un volúmen de repetición de código impresionante.

El enfoque para el escenario ideal se denomina **Capa de Servicios** y es parte de la arquitectura de **Domain Model Logic**, empleada en la metodología de software **MDD** Este escenario se conoce en su totalidad como **SOA** o **Service Oriented Architecture**.

Una Capa de Servicio es un **Patrón de Arquitectura Enterprise** que permite encapsular la lógica de la aplicación detrás de una interface simple (o límite / Boundary) según definido por Martin Fowler.
En dicha interfaz, las operaciones están protegidas del exterior y disueltas en lo que se conoce como **Loose Coupling**, dónde todas las operaciones se realizan internamente y es relativamente sencillo construir servicios complejos que pueden ser consumidos por diferentes frontends o capas de cliente.

Desde luego, todos estos conceptos deben ser respaldados por modelos funcionales, que es de lo que se trata el presente tutorial. A través del cual, estaremos desarrollando una arquitectura de servicios paso a paso, con indicaciones específicas de cómo implementar cada fragmento de la arquitectura.
Dicha arquitectura es considerada en el 2014 como el modelo fundamental de construcción de aplicaciones web.

### Controlador Convencional VS Controlador con Servicio ###

    <?php
    /* 
     * Sin Arquitectura de Servicio
     * (UserController.php)
     */

    /*
     * Controlador de Usuarios
     */
    class UserController
    {
        protected $_userModel;
        protected $_view;

        /**
         * Constructor
         */
        public function __construct()
        {
            $this->_userModel = ModelHelper::getModel(‘users’);
        }

        /**
         * Traer todos los usuarios
         */  
        public function indexAction()
        {
            $users = $this->_userModel->fetchAll();
            $this->_view->users = $users;
            $this->_view->render();
        }

         /**
          * Salvar un nuevo usuario
          */
         public function insertAction(array $data)
         {
             $this->_userModel->insert($data);
             //redireccionar a indexAction / vista
         }

         /**
          * Actualizar un usuario
          */
         public function updateAction($id, $data)
         {
             $this->_userModel->update($id, $data);
             //redireccionar a indexAction / vista
         }

         /**
          * Borrar un usuario
          */
         public function deleteAction($id)
         {
             $this->_userModel->delete($id);
             //redireccionar a indexAction / vista
         }
    }


    <?php
    /* 
     * Con Arquitectura de Servicio
     * (UserController.php)
     */

    /*
     * Controlador de Usuarios
     */
    class UserController
    {
        protected $_userService;
        protected $_view;

        /**
         * Constructor
         */
        public function __construct()
        {
            $this->_userService = ServiceHelper::getService(‘users’);
        }

        /**
         * Traer todos los usuarios
         */  
        public function indexAction()
        {
            $users = $this->_userService->fetchAll();
            $this->_view->users = $users;
            $this->_view->render();
        }

         /**
          * Salvar a un usuario, puede venir de POST, WS, GET, REST
          */
         public function insertAction(array $data)
         {
             $this->_userService->insert($data);
             //redireccionar a indexAction / vista
         }

         /**
          * Actualizar un usuario, desde POST, WS, GET o REST
          */
         public function updateAction($id, $data)
         {
             $this->_userService->update($id, $data);
             //redireccionar a indexAction / vista
         }

         /**
          * Borrar un usuario específico
          */
         public function deleteAction($id)
         {
             $this->_userService->delete($id);
             //redireccionar a indexAction / vista
         }   
    }

Tal vez los cambios parezcan sutiles, pero la implementación es radicalmente distinta. Ahora, la clase de controlador consume un objeto de servicio, que es el responsable de las operaciones CRUD. Y no hay interacción directa con ningún modelo y ninguna pista de cómo se implementan dichas operaciones.
Aún mas, el servicio podría ser usado en otro controlador o capa de cliente y no habría ninguna duplicidad de código en ningún lado.

### MVC y Capa de Dominio (Domain Layer) ###

Aquí implementaremos nuestro primer código, una capa de dominio que es la escencia del patrón DML en conjunto con el patrón MVC.

####Entidad Abstracta Base####

El primer paso hacia la implementación de una Capa de Servicios es crear un Modelo de Dominio (**Domain Model**) básico, que contendrá algunos objetos base para cualquier entidad.
Esto se logra implementando una clase abastacta, que sirve como clase base para todos los demás modelos o entidades:

    <?php
    /*
     * AbstractEntity.php
     */
    namespace Entity;
    
    abstract class AbstractEntity
    {
        protected $_values = array();
        protected $_allowedFields = array();

        /**
         * Constructor base
         */
        public function __construct(array $data)
        {
            foreach ($data as $name => $value) {
                $this->$name = $value;
            }
        }

        /**
         * Asigna el valor al campo a través del mutador (**mutator**) correspondiente si existe;
         * de lo contrario, asigna el valor directamente que sea pasado
         */
        public function __set($name, $value)
        {  
            if (!in_array($name, $this->_allowedFields)) {
                throw new EntityException(‘El campo ‘ . $name . ‘ no está permitido en esta entidad.’); 
            }
            $mutator = ‘set’ . ucfirst($name);
            if (method_exists($this, $mutator) && is_callable(array($this, $mutator))) {
                $this->$mutator($value);
            }
            else {
                $this->_values[$name] = $value;
            }   
        }

        /**
         * Obtiene el valor del campo requerido a través del accesor (**accesor**) correspondiente;
         * de lo contrario, lo obtiene directamente de la propiedad
         */
        public function __get($name)
        {
            if (!in_array($name, $this->_allowedFields)) {
                throw new EntityException(‘El campo ‘ . $name . ‘ no está permitido en esta entidad.’);
            }
            $accessor = ‘get’ . ucfirst($name);
            if (method_exists($this, $accessor) && is_callable(array($this, $accessor))) {
                return $this->$accessor;   
            }
            if (isset($this->_values[$name])) {
                return $this->_values[$name];  
            }
            throw new EntityException(‘El campo ‘ . $name . ‘ no ha sido establecido para esta entidad.’);
        }

        /**
         * Revisa si el campo especificado ha sido establecido
         */
        public function __isset($name)
        {
            if (!in_array($name, $this->_allowedFields)) {
                throw new EntityException(‘El campo ‘ . $name . ‘ no está permitido en esta entidad.’);
            }
            return isset($this->_values[$name]);
        }

        /**
         * Elimina el valor de un campo
         */
        public function __unset($name)
        {
            if (!in_array($name, $this->_allowedFields)) {
                throw new EntityException(‘El campo ‘ . $name . ‘ no está permitido en esta entidad.’);
            }
            if (isset($this->_values[$name])) {
                unset($this->_values[$name]);
            }
        }

        /**
         * Obtiene un array asociativo de los valores asignados a los campos en la entidad.
         */
        public function toArray()
        {
            return $this->_values;
        }
    }

Ahora declararemos la clase de la excepción

<?php
    /*
     * EntityException.php
     */
    namespace MyApplicationEntity;
    
    class EntityException extends Exception{}

Cmo se mostró anteriormente, la clase *AbstractEntity* es similar a otras que actúan como clases base o modelos de herencia. Implementa los métodos mágicos de Php (**Magic Methods**) para establecer setters y getters dinámicos.
Esta clase abstracta, corresponde al modelo mínimo de clase base para implementar modelos. A continuación crearemos una entidad, que herede de dicha clase, a modo de ejemplo.

####Modelado de Entidades####

Para crear nuestra primera entidad, que herede de nuestra clase abstracta de entidades, tomaremos de muestra el famoso objeto User.


    <?php
    /*
     * User.php
     */
    
    namespace MyApplicationEntity;
    
    class User extends AbstractEntity
    {  
        protected $_allowedFields = array(‘id’, ‘fname’, ‘lname’, ‘email’);
        
        /**
         * Establecer Id
         */
        public function setId($id)
        {
            if(!filter_var($id, FILTER_VALIDATE_INT, array(‘options’ => array(‘min_range’ => 1, ‘max_range’ => 999999)))) {
                throw new EntityException(‘El ID especificado es inválido.’);
            }
            $this->_values['id'] = $id;
        }
        
        /**
         * Establecer nombres de usuario
         */ 
        public function setFname($fname)
        {
            if (strlen($fname) < 2 || strlen($fname) > 32) {
                throw new EntityException(‘El nombre es inválido.’);
            }
            $this->_values['fname'] = $fname;
        }
        
        /**
         * Establecer los apellidos del usuario
         */
        public function setLname($lname)
        {
            if (strlen($lname) < 2 || strlen($lname) > 32) {
                throw new EntityException(‘Los apellidos son inválidos.’);
            }
            $this->_values['lname'] = $lname;
        }
        
        /**
         * Establecer el mail del usuario
         */
        public function setEmail($email)
        {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new EntityException(‘El mail especificado es inválido.’);
            }
            $this->_values['email'] = $email;
        }
    }

Esta clase es simplemente un modelo, que representa al objeto Usuario con sus distintos campos. Implementa la clase abstracta, por lo que hereda todos sus **mutators** y por ende podrá establecer los valores para todas sus propiedades.
A esta configuración se de dominia Modelo de Dominio, que es la representación básica de una entidad. Esta capa no esta pensada para implementar reglas de negocio, solo para representar objetos. Adicionalmente requeriremos mapeo de datos (**Data Mapping Layer**) y la capa de acceso a datos (**Data Access Layer**).